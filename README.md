# Minimal Network Solution - #107 Euler Project
This solution uses networkx to build a Graph and Minimum Spanning Tree. 

Afterwards the size of both the graph and tree are compared to get the answer.

#

## Setup
Setup the virtual enviroment so we don't install networkx onto our computer.

```
python -m venv venv
```

Enter the virtual environment
```
FOR WINDOWS:
.\venv\Scripts\activate

FOR LINUX:
source venv/bin/activate
```

Install networkx
```
pip install networkx
```

Then run the code.
```
python minimalNetwork.py
```
