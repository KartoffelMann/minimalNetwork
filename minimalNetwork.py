from networkx import minimum_spanning_tree, Graph
import csv

input = 'p107_network.txt'
data = [ line for line in csv.reader(open(input, "r")) ]

ROW_MAX = len(data)
COL_MAX = len(data[0])

# Empty graph that we'll fill with the edges from the file
graph = Graph()

# Insert into graph
for row in range(ROW_MAX):
    for col in range(COL_MAX):
        if data[row][col] != '-':
            graph.add_edge(row+1, col+1, weight=int(data[row][col]) )

# Build minimum spanning tree from graph, MST contains optimal network
tree = minimum_spanning_tree(graph, weight='weight')

# Compare the size of the graph to the MST
savings = graph.size(weight='weight') - tree.size(weight='weight')

print(int(savings))